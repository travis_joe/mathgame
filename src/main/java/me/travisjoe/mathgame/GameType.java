package me.travisjoe.mathgame;

/**
 * Created by travis on 9/10/15.
 */
public enum GameType {
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION
}
