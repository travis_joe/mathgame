package me.travisjoe.mathgame;

import asg.cliche.*;
import me.travisjoe.mathgame.menus.MainMenu;

import java.io.IOException;

/**
 * Created by travis on 9/10/15.
 */
public class MathGame {

    public static void main(String args[]) throws IOException {
        ShellFactory.createConsoleShell("MathGame", "Use ?list for a list of commands.", new MainMenu()).commandLoop();
    }

}
