package me.travisjoe.mathgame.menus;

import asg.cliche.*;
import me.travisjoe.mathgame.Game;
import me.travisjoe.mathgame.GameType;
import me.travisjoe.mathgame.MathPlayer;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by travis on 9/10/15.
 */
public class MainMenu implements ShellDependent {
    private Shell shell;
    private MathPlayer player;

    public void cliSetShell(Shell shell) {
        this.shell = shell;
    }

    @Command(name="start")
    public void startGame() throws IOException {
        // TODO: Check if a saved game exists before trying to create a new one
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your name: ");
        String name = scanner.nextLine();
        player = new MathPlayer(name);

        System.out.print("Choose a game type: (addition, subtraction) ");
        String gameString = scanner.nextLine();
        GameType type;
        try {
            type = GameType.valueOf(gameString.toUpperCase());
            if(type == GameType.ADDITION) {
                Game game = new AdditionGame(player);
                ShellFactory.createSubshell("AdditionGame", shell, "", game).commandLoop();
            } else if(type == GameType.SUBTRACTION) {
                Game game = new SubtractionGame(player);
                ShellFactory.createSubshell("SubtractionGame", shell, "", game).commandLoop();
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid game type!");
        }
    }

    @Command
    public void save() {
        if(player == null) {
            System.out.println("You can't save before starting the game!");
            return;
        }
        try {
            FileUtils.writeStringToFile(new File(player.getName() + ".txt"), player.toString());
            System.out.println("Saved player '" + player.getName() + "' successfully!");
        } catch (IOException e) {
            System.out.println("Failed to save data!");
            System.out.println(e.getMessage());
        }
    }

    @Command
    public void load(String name) {
        try {
            String data = FileUtils.readFileToString(new File(name + ".txt"));
            player = MathPlayer.fromString(data);
            System.out.println("Loaded player '" + player.getName() + "' successfully!");
        } catch (IOException e) {
            System.out.println("Failed to load data!");
            System.out.println(e.getMessage());
        }
    }

    @Command
    public void load() {
        File dir = new File(".");
        System.out.println("load <saveName>");
        for(File f : dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".txt");
            }
        })) {
            System.out.println(f.getName().replace(".txt", ""));
        }
    }

    @Command(name = "stats")
    public void viewStats() {
        if(player == null) {
            System.out.println("Cannot view stats, no player loaded!");
            return;
        }

        System.out.println(player.getName() + "'s statistics");
        System.out.println("Score: " + player.getScore());

    }
}