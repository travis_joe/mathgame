package me.travisjoe.mathgame.menus;

import asg.cliche.Command;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.travisjoe.mathgame.Game;
import me.travisjoe.mathgame.MathPlayer;

import java.util.Random;

/**
 * Created by travis on 9/10/15.
 */

public class AdditionGame extends Game {

    public AdditionGame(MathPlayer player) {
        this.player = player;
        generateQuestion();
    }

    protected void generateQuestion() {
        generateQuestion(2, 250);
    }

    protected void generateQuestion(int terms, int max) {
        Random r = new Random();
        for(int i = 0; i < terms; i++) {
            numbers[i] = r.nextInt(max);
        }
        showQuestion();
    }

    protected void showQuestion() {
        String question = "";
        for(int i = 0; i < numbers.length; i++) {
            if(i == numbers.length - 1) {
                question = question.concat(numbers[i] + " = ?");
            } else {
                question = question.concat(numbers[i] + " + ");
            }
        }
        System.out.println(question);
    }

    protected boolean validateInput(int ans) {
        int sum = 0;
        for(int i : numbers) {
            sum += i;
        }

        if(ans == sum) {
            return true;
        }

        return false;
    }
}
