package me.travisjoe.mathgame;

import asg.cliche.Command;
import lombok.Getter;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by travis on 9/10/15.
 */
public abstract class Game {
    @Getter
    protected MathPlayer player;

    @Getter
    protected int[] numbers = {0, 0};

    @Command(abbrev = "=")
    public void answer(int ans) {
        if(validateInput(ans)) {
            System.out.println("Correct! You earned 10 points!");
            questionCorrect();
        } else {
            System.out.println("Wrong! You lost 5 points!");
            questionIncorrect();
        }
        System.out.println("Your score: " + player.getScore());
        generateQuestion();
    }

    @Command
    public void skip() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Are you sure you want to skip this question? You will lose 5 points. (Y/N)?");
        String input = scanner.nextLine();
        if(input.equalsIgnoreCase("y")) {
            questionSkipped();
            System.out.println("Question skipped! Your score is now " + player.getScore());
            generateQuestion();
        }
    }

    protected void generateQuestion() {
        generateQuestion(2, 50);
    }

    protected void generateQuestion(int terms, int max) {
        Random r = new Random();
        for(int i = 0; i < terms; i++) {
            numbers[i] = r.nextInt(max);
        }
        showQuestion();
    }

    protected abstract boolean validateInput(int ans);

    protected abstract void showQuestion();

    protected void questionSkipped() {
        player.skippedQuestion();
    }

    protected void questionCorrect() {
        player.correctQuestion();
    }

    protected void questionIncorrect() {
        player.incorrectQuestion();
    }
}
