package me.travisjoe.mathgame;

import lombok.*;

/**
 * Created by travis on 9/10/15.
 */

@RequiredArgsConstructor
public class MathPlayer {
    @Getter @NonNull
    private String name;
    @Getter
    @Setter(AccessLevel.PRIVATE)
    private int score;
    @Getter
    @Setter(AccessLevel.PRIVATE)
    private int skipped;
    @Getter
    @Setter(AccessLevel.PRIVATE)
    private int correct;
    @Getter
    @Setter(AccessLevel.PRIVATE)
    private int incorrect;

    private void givePoints(int points) {
        score += points;
    }

    private void takePoints(int points) {
        score -= points;
    }

    public void skippedQuestion() {
        takePoints(5);
        skipped += 1;
    }

    public void correctQuestion() {
        givePoints(10);
        correct += 1;
    }

    public void incorrectQuestion() {
        takePoints(5);
        incorrect += 1;
    }

    public String toString() {
        return name + ";" + score + ";" + skipped + ";" + correct + ";" + incorrect;
    }

    public static MathPlayer fromString(String string) {
        String[] tmp = string.split(";");
        String name = tmp[0];
        String scoreStr = tmp[1];
        String skippedStr = tmp[2];
        String correctStr = tmp[3];
        String incorrectStr = tmp[4];
        int score = 0;
        int skipped = 0;
        int correct = 0;
        int incorrect = 0;
        try {
            score = Integer.parseInt(scoreStr);
            skipped = Integer.parseInt(skippedStr);
            correct = Integer.parseInt(correctStr);
            incorrect = Integer.parseInt(incorrectStr);
        } catch (NumberFormatException e) {
            System.out.println("ERROR: Failed to parse data. Score may be lost.");
        }
        MathPlayer player = new MathPlayer(name);
        player.setScore(score);
        player.setSkipped(skipped);
        player.setCorrect(correct);
        player.setIncorrect(incorrect);
        return player;
    }
}
