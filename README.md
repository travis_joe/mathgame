You'll need maven. (https://maven.apache.org/)

Clone the repo, then run
```
#!

mvn clean install
```
...Or download the jar from the Downloads tab.